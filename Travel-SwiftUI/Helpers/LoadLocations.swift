//
//  LoadLocations.swift
//  Travel-SwiftUI
//
//  Created by Shakoushy on 25/01/2021.
//

import Foundation

class LoadLocations: ObservableObject {
    let places: [Location]
    
    var primary: Location {
        places[0]
    }
    
    init() {
        let url = Bundle.main.url(forResource: "locations", withExtension: "json")!
        let data = try! Data(contentsOf: url)
        places = try! JSONDecoder().decode([Location].self, from: data)
    }
}
