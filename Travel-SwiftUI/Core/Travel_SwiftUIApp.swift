//
//  Travel_SwiftUIApp.swift
//  Travel-SwiftUI
//
//  Created by Shakoushy on 20/01/2021.
//

import SwiftUI

@main
struct Travel_SwiftUIApp: App {
    @StateObject var locations = LoadLocations()

    var body: some Scene {
        WindowGroup {
            TabView {
                NavigationView {
                    DiscoverView(location: locations.primary)
                }
                .tabItem {
                    Image(systemName: "airplane.circle.fill")
                    Text("Discover")
                }
                
                NavigationView {
                    WorldView()
                }
                .tabItem {
                    Image(systemName: "mappin.circle.fill")
                    Text("Locations")
                }
                
                NavigationView {
                    TipsView()
                }
                .tabItem {
                    Image(systemName: "pencil.circle.fill")
                    Text("Tips")
                }
            }
            .environmentObject(locations)
        }
    }
}
