//
//  Tip.swift
//  Travel-SwiftUI
//
//  Created by Shakoushy on 26/01/2021.
//

import Foundation

struct Tip: Decodable {
    let text: String
    let children: [Tip]?
}
