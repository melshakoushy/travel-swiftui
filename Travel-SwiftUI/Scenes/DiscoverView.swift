//
//  ContentView.swift
//  Travel-SwiftUI
//
//  Created by Shakoushy on 20/01/2021.
//

import SwiftUI

struct DiscoverView: View {
    let location: Location
    
    var body: some View {
        ScrollView {
            Image(location.heroPicture)
                .resizable()
                .scaledToFit()
            Text(location.name)
                .font(.largeTitle)
                .bold()
                .multilineTextAlignment(.center)
            Text(location.country)
                .font(.title)
                .foregroundColor(.secondary)
            Text(location.description)
                .padding(.horizontal)
            Text("Did you know?")
                .bold()
                .font(.title3)
                .padding(.top)
            Text(location.more)
                .padding(.horizontal)

        }
        .navigationTitle("Discover")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            DiscoverView(location: LoadLocations().primary)
        }
    }
}
